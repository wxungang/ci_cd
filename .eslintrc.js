module.exports = {
    root: true,
    parser: 'babel-eslint', //not for vue
    parserOptions: {
        // "parser": "babel-eslint",//for vue put in here!
        ecmaVersion: 6,
        sourceType: 'module',
        impliedStrict: true,
        ecmaFeatures: {
            jsx: true
        }
    },
    env: {
        browser: true,
        es6: true
    },
    globals: {
        require: true,
        module: true,
        exports: true
    },
    extends: ['eslint:recommended', 'plugin:flowtype/recommended', 'plugin:react/recommended'],
    plugins: ['react', 'flowtype'],
    settings: {
        react: {
            createClass: 'createReactClass', // Regex for Component Factory to use,
            // default to "createReactClass"
            pragma: 'React', // Pragma to use, default to "React"
            fragment: 'Fragment', // Fragment to use (may be a property of <pragma>), default to "Fragment"
            version: 'detect', // React version. "detect" automatically picks the version you have installed.
            // You can also use `16.0`, `16.3`, etc, if you want to override the detected value.
            // default to latest and warns if missing
            // It will default to "detect" in the future
            flowVersion: 'detect' // Flow version
        },
        flowtype: {
            onlyFilesWithFlowAnnotation: true //只检查 声明 flow语法的文件
        }
    },
    rules: {
        camelcase: 1,
        'no-alert': 2, //
        'no-console': 0,
        'no-labels': 2, //
        'no-multi-str': 2, //多行字符串
        'no-sequences': 2, //逗号运算符 var c=a,b;
        //Best Practices
        'no-with': 2,
        'no-caller': 2,
        'no-eval': 2,
        'no-eq-null': 2,
        'no-unused-vars': 2, //未使用的变量
        'no-undef': 2, //no undefined
        eqeqeq: 2, //===
        'block-scoped-var': 2, //块级内禁止使用var
        'vars-on-top': 1, //
        curly: [2, 'all'], //if(){ }
        //ES6
        'no-var': 2, //使用let const
        //Stylistic Issues
        'one-var': 0
    }
};
