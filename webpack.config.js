'use strict';
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    // 报错直接终止
    bail: true,
    mode: 'production',
    devtool: false,
    context: __dirname,
    entry: {
        app: path.join(__dirname, './src/ci_cd.js')
    },
    output: {
        path: path.join(__dirname, './build'),
        publicPath: '',
        filename: '[name].[chunkhash].js'
    },
    resolve: {},
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [path.join(__dirname, './build')],
            cleanAfterEveryBuildPatterns: [path.join(__dirname, './temp')]
        })
    ],
    performance: {
        maxEntrypointSize: 400000
    }
};
