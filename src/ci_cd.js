// @flow
/**
 *
 * @param {number} a
 * @param {number} b
 */
function sum(a: number, b: number): number {
    return a + b;
}
module.exports = {
    sum
};
