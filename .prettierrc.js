// prettier.config.js or .prettierrc.js
module.exports = {
	// 末尾加逗号 eg: [1,2,] or {a:1,b:2,}
	trailingComma: 'none',
	tabWidth: 4,
	semi: true,
	// 单引号
	singleQuote: true,
	printWidth: 250,
};
